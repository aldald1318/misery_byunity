﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotToolTip : MonoBehaviour
{
    // Components
    [SerializeField]
    private GameObject go_Base;

    [SerializeField]
    private Text text_ItemName;
    [SerializeField]
    private Text text_ItemDesc;
    [SerializeField]
    private Text text_ItemHowtoUsed;

    public void ShowToolTip(Item item, Vector3 pos)
    {
        go_Base.SetActive(true);
        pos += new Vector3(go_Base.GetComponent<RectTransform>().rect.width *0.5f, -go_Base.GetComponent<RectTransform>().rect.height, 0f);
        go_Base.transform.position = pos;

        text_ItemName.text = item.itemName;
        text_ItemDesc.text = item.itemDesc;

        if (item.itemType == Item.ItemType.Equipment)
            text_ItemHowtoUsed.text = "우클릭 - 장착";
        else if (item.itemType == Item.ItemType.Used)
            text_ItemHowtoUsed.text = "우클릭 - 먹기";
        else
            text_ItemHowtoUsed.text = "";
    }

    public void HideToolTip()
    {
        go_Base.SetActive(false);
    }
}
