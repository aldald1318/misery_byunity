﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Pig : WeakAnimal
{
    protected override void ResetState()
    {
        base.ResetState();
        RandomAction();
    }

    private void RandomAction()
    {
        RandomSound();
        int random = Random.Range(0, 4); // 대기, 풀뜯기, 두리번, 걷기

        switch (random)
        {
            case 0:
                Wait();
                break;
            case 1:
                Eat();
                break;
            case 2:
                Peek();
                break;
            case 3:
                TryWalk();
                break;
        }
    }

    private void Wait()
    {
        currentTime = waitTime;
        Debug.Log("대기");
    }

    private void Eat()
    {
        currentTime = eatTime;
        anim.SetTrigger("Eat");
        Debug.Log("풀뜯기");
    }

    private void Peek()
    {
        currentTime = peekTime;
        anim.SetTrigger("Peek");
        Debug.Log("두리번");
    }
}
