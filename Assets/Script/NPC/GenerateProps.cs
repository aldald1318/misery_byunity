﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateProps : MonoBehaviour
{
    [SerializeField]
    private int mapX;
    [SerializeField]
    private int mapZ;

    [SerializeField]
    private GameObject theRock;
    [SerializeField]
    private int rockCount;

    // Start is called before the first frame update
    void Start()
    {
        RandomSpawn();    
    }

    void RandomSpawn()
    {
        // 나중에 데이터베이스에서 꺼내와 수정할 예정
        // 현재 하드코딩중
        for(int i=0; i<rockCount; ++i)
        {
            int randomPosX = Random.Range(0, mapX);
            int randomPosY = Random.Range(0, mapZ);
            Instantiate(theRock, new Vector3(randomPosX, 0, randomPosY), Quaternion.identity);
        }
    }
}
