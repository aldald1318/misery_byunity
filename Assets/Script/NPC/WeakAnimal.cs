﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeakAnimal : Animal
{
    // 위협을 주는 플레이어의 반대방향으로 뜀
    public void Run(Vector3 targetPos)
    {
        destination = new Vector3(transform.position.x - targetPos.x, 0f, transform.position.z - targetPos.z).normalized;
        currentTime = runTime;
        isWalking = false;
        isRunning = true;
        nav.speed = runSpeed;
        anim.SetBool("Running", isRunning);
    }

    public override void Damage(int dmg, Vector3 targetPos)
    {
        base.Damage(dmg, targetPos);
        if (!isDead)
            Run(targetPos);
    }
}
