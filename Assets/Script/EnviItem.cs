﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnviItem : MonoBehaviour
{
    [SerializeField]
    protected int hp;

    [SerializeField]
    protected float destroyTime; // 파편 제거 시간

    [SerializeField]
    protected SphereCollider col; // 구체 콜라이더

    // Components
    [SerializeField]
    protected GameObject go_enviItem;
    [SerializeField]
    protected GameObject go_debris; // 깨진바위
    [SerializeField]
    protected GameObject go_effect_prefabs; // 깨진바위
    [SerializeField]
    protected GameObject go_enviItem_prefabs; // 돌맹이 아이템

    [SerializeField]
    protected int itemMinCount;
    [SerializeField]
    protected int itemMaxCount;

    // 필요한 사운드 이름
    [SerializeField]
    protected string strike_Sound;
    [SerializeField]
    protected string destroy_Sound;

    public void Mining()
    {
        SoundManager.instance.PlaySE(strike_Sound);
        var clone = Instantiate(go_effect_prefabs, col.bounds.center, Quaternion.identity);
        Destroy(clone, destroyTime);

        hp--;

        if (hp <= 0)
            Destruction();
    }

    protected void Destruction()
    {
        SoundManager.instance.PlaySE(destroy_Sound);

        col.enabled = false;
        for (int i = 0; i < Mathf.Round(Random.Range(itemMinCount, itemMaxCount)); ++i)
        {
            Instantiate(go_enviItem_prefabs, go_enviItem.transform.position, Quaternion.identity);
        }
        Destroy(go_enviItem);

        go_debris.SetActive(true);
        Destroy(go_debris, destroyTime);
    }
}
