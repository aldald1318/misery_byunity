﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public string gunName;
    public float range;
    public float accuracy;
    public float fireRate;
    public float reloadTime;

    public int damage; // 총의 데미지
    public int reloadBulletCount; // 총알 재장전 개수
    public int currentBulletCount; // 현재 탄알 개수
    public int maxBulletCount; // 최대 소유 가능총알 개수
    public int carryBulletCount; // 현재 소유하고 있는 총알개수

    public float retroActionForce; // 반동세기
    public float retroActionFineSightForce; // 정조준시의 반동세기

    public Vector3 fineSightOriginPos;
    public Animator anim;
    public ParticleSystem muzzleFlash;

    public AudioClip fire_Sound;
}
