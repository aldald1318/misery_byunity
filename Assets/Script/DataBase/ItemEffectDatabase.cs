﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemEffect
{
    public string itemName; // 이름(키값)
    [Tooltip("HP, SP, DP, HUNGRY, THIRSTY, SATISFY만 가능합니다.")]
    public string[] part;   // 부위
    public int[] num;   // 수치
}


public class ItemEffectDatabase : MonoBehaviour
{
    [SerializeField]
    private ItemEffect[] itemEffects;

    // Components
    [SerializeField]
    private StatusController thePlayerStatus;
    [SerializeField]
    private WeaponManager theWeaponManager;
    [SerializeField]
    private SlotToolTip theSlotToolTip;

    private const string HP = "HP", SP = "SP", DP = "DP", HUNGRY = "HUNGRY", THIRSTY = "THIRSTY", SATISFY = "SATISFY";

    public void ShowToolTip(Item item, Vector3 pos)
    {
        theSlotToolTip.ShowToolTip(item, pos);
    }

    public void HideToolTip()
    {
        theSlotToolTip.HideToolTip();
    }

    public void UseItem(Item item)
    {
        // 장비
        if (item.itemType == Item.ItemType.Equipment)
        {
            // 장착
            StartCoroutine(theWeaponManager.ChangeWeaponCoroutine(item.weaponType, item.itemName));
        }
        // 소모품
        else if (item.itemType == Item.ItemType.Used)
        {
            for (int i = 0; i < itemEffects.Length; i++)
            {
                if(itemEffects[i].itemName == item.itemName)
                {
                    for(int y=0; y < itemEffects[i].part.Length; y++)
                    {
                        switch (itemEffects[i].part[y])
                        {
                            case HP:
                                thePlayerStatus.IncreaseHP(itemEffects[i].num[y]);
                                break;
                            case SP:
                                thePlayerStatus.IncreaseSP(itemEffects[i].num[y]);
                                break;
                            case DP:
                                thePlayerStatus.IncreaseDP(itemEffects[i].num[y]);
                                break;
                            case THIRSTY:
                                thePlayerStatus.IncreaseThirsty(itemEffects[i].num[y]);
                                break;
                            case HUNGRY:
                                thePlayerStatus.IncreaseHungry(itemEffects[i].num[y]);
                                break;
                            case SATISFY:
                                thePlayerStatus.IncreaseHP(itemEffects[i].num[y]);
                                break;
                            default:
                                Debug.Log("잘못된 Status 부위를 적용시키려고 하고있습니다.HP, SP, DP, HUNGRY, THIRSTY, SATISFY만 가능합니다.");
                                break;
                        }
                        Debug.Log(item.itemName + "을 사용했습니다.");
                    }
                    return;
                }
            }
            Debug.Log("ItemEffectDatabase에 일치하는 itemName이 없습니다.");
        }
    }
}
